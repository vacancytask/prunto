<?php

namespace frontend\controllers;


use frontend\models\Outer\RequestDispatcher;
use frontend\models\Separator;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use function json_encode;


class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['split', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'split' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function actionSplit(): string
    {
        $dispatcher = new RequestDispatcher();
        $model = new Separator();

        $answer = $dispatcher->getSplitResult($model);

        return $answer;
    }

    public function actionLogin(): string
    {
        $result = RequestDispatcher::login();

        $jsonAnswer = json_encode($result);

        return $jsonAnswer;
    }

    public function actionLogout(): string
    {

        $result = RequestDispatcher::logout();

        $jsonAnswer = json_encode($result);

        return $jsonAnswer;
    }

}
