<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "separator".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $symbols
 * @property string $pattern
 * @property string $split_at
 * @property string $processed_date
 *
 * @property User $user
 */
class Separator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'separator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'split_at'], 'integer'],
            [['symbols', 'pattern'], 'string'],
            [['split_at', 'processed_date'], 'required'],
            [['processed_date'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'symbols' => 'Массив символов',
            'pattern' => 'Символ для разбиения',
            'split_at' => 'Индекс с которого начинается вторая часть',
            'processed_date' => 'Рассчитано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->inverseOf('splitters');
    }
}
