<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 03.11.2017 Time: 23:29
 */

namespace frontend\models\BusinessProcess;


use frontend\models\Separator;
use Yii;
use const true;

class BusinessUnit
{
    const DATE_FORMAT = 'Ymd h:i:s';


    /** @var Task задача для выполнения */
    private $task = null;
    /** @var Product результат выполнения задачи */
    private $product = null;

    function setTask(Task $task)
    {
        $this->task = $task;
    }

    function executeTask(): bool
    {

        $isValid = $this->task instanceof Task;

        $isSuccess = false;
        if ($isValid) {

            $rawString = $this->task->getAttribute(Task::SYMBOLS);
            $pattern = $this->task->getAttribute(Task::PATTERN);

            $answer = $this->getAnswer($rawString, $pattern);

            $product = new Product();
            $product->setAttribute(Product::ANSWER, $answer);

            $this->product = $product;

            $isSuccess = true;

        }

        if ($isSuccess && $isValid) {
            $this->task->finish = true;
            $this->task->success = true;

        }

        if (!$isSuccess && $isValid) {

            $this->task->finish = true;
            $this->task->success = false;
        }

        return $isSuccess;
    }

    /**
     * @param $rawString
     * @param $pattern
     * @return int
     */
    private function getAnswer(string $rawString, string $pattern): int
    {
        $splitter = new Splitter($rawString);
        $answer = $splitter->choose($pattern);

        return $answer;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function saveWork(): ?int
    {
        $isValid = $this->task instanceof Task;

        $rawString = '';
        $pattern = '';
        if ($isValid) {

            $rawString = $this->task->getAttribute(Task::SYMBOLS);
            $pattern = $this->task->getAttribute(Task::PATTERN);
        }

        $answer = null;
        $isValid = $this->product instanceof Product;
        if ($isValid) {

            $answer = $this->product->getAttribute(Product::ANSWER);
        }

        $isValid = !empty($rawString) && !empty($pattern) && !empty($answer);

        $isSuccess = false;
        $separator = new Separator();
        if ($isValid) {

            $separator->symbols = $rawString;
            $separator->pattern = $pattern;
            $separator->split_at = $answer;
            $separator->processed_date = date(self::DATE_FORMAT);
            $separator->user_id = Yii::$app->user->identity->getId();

            $isSuccess = $separator->save();

        }

        $workId = null;
        if ($isSuccess) {
            $workId = $separator->id;
        }

        return $workId;
    }

}
