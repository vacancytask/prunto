<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 03.11.2017 Time: 23:31
 */

namespace frontend\models\BusinessProcess;


use frontend\models\Core\Storage;

class Task extends Storage
{

    const SYMBOLS = 'symbols';
    const PATTERN = 'pattern';
    const ANSWER = 'answer';
    const WORK_ID = 'result_id';

    public $finish = false;
    public $success = true;

}
