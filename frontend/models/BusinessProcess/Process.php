<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 03.11.2017 Time: 23:44
 */

namespace frontend\models\BusinessProcess;


use function json_encode;


class Process
{
    const SPLIT_AT = 'split_at_index';
    const WORK = 'work_id';

    function split($customerRequest): string
    {

        $customer = new Customer($customerRequest);
        $task = $customer->getSplitTask();

        $unit = new BusinessUnit();

        $unit->setTask($task);
        $isSuccess = $unit->executeTask();

        $answer = null;
        if ($isSuccess) {
            $product = $unit->getProduct();
            $answer = $product->getAttribute(Product::ANSWER);
        }

        $workId = null;
        if ($isSuccess) {
            $workId = $unit->saveWork();
        }

        $response = array(
            self::SPLIT_AT => $answer,
            self::WORK => $workId
        );
        $output = json_encode($response);

        return $output;


    }

}
