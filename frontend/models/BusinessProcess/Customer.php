<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 03.11.2017 Time: 23:35
 */

namespace frontend\models\BusinessProcess;


use frontend\models\Separator;

class Customer
{

    const SYMBOLS = 'symbols';
    const PATTERN = 'pattern';

    private $separator;

    function __construct(Separator $customerRequest)
    {
        $this->separator = $customerRequest;
    }

    function getSplitTask(): Task
    {

        $rawString = $this->separator[self::SYMBOLS];
        $pattern = $this->separator[self::PATTERN];

        $customerTask = new Task();

        $customerTask->setAttribute(Task::SYMBOLS, $rawString);
        $customerTask->setAttribute(Task::PATTERN, $pattern);

        return $customerTask;

    }
}
