<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 03.11.2017 Time: 0:10
 */

namespace frontend\models\BusinessProcess;


class Chooser
{
    private $elements = array();
    private $markup = array();
    private $pattern = '';

    function __construct(string $pattern, array $elements, array $markup)
    {
        $this->elements = $elements;
        $this->markup = $markup;
        $this->pattern = $pattern;
    }

    function makeDecision():?int
    {

        $choosenOne = null;

        $differentNumber = 0;
        $isConsensus = false;
        $searching = true;
        $markupSize = count($this->markup) - 1;
        $index = $markupSize;
        while ($searching) {

            $isDifferent = $this->elements[$index] != $this->pattern;
            if ($isDifferent) {
                $differentNumber++;
            }

            $isConsensus = $this->markup[$index] == $differentNumber;
            if ($isConsensus) {
                $searching = false;
            }

            $mayContinue = $searching && ($index > 0);

            if ($mayContinue) {
                $index--;
            }
            if (!$mayContinue) {
                $searching = false;
            }

        }

        if ($isConsensus) {
            $choosenOne = $index;
        }

        return $choosenOne;
    }

}
