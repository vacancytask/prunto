<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 04.11.2017 Time: 1:39
 */

namespace frontend\models\BusinessProcess;


use frontend\models\Core\Storage;

class Product extends Storage
{
    const ANSWER = 'answer';
    const WORK_ID = 'work_id';
}
