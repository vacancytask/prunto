<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 02.11.2017 Time: 23:41
 */

namespace frontend\models\BusinessProcess;

class Splitter
{
    const BLANK = ' ';
    const DELIMITER = ',';
    const CHOOSE_FAIL = -1;

    private $elements = array();

    function __construct(string $manySymbols, string $blank = self::BLANK, string $delimiter = self::DELIMITER)
    {
        $withoutBlank = str_replace($blank, '', $manySymbols);
        $separateSymbols = explode($delimiter, $withoutBlank);

        $this->elements = $separateSymbols;
    }

    function choose(string $pattern): int
    {
        $splitAtPosition = self::CHOOSE_FAIL;

        $elementsMarkup = $this->markupElements($pattern);

        $chooser = new Chooser($pattern, $this->elements, $elementsMarkup);
        $choosenOne = $chooser->makeDecision();

        $isFail = is_null($choosenOne);
        if (!$isFail) {
            $splitAtPosition = $choosenOne;
        }

        return $splitAtPosition;
    }

    /**
     * @param string $pattern
     * @return array
     * @internal param $matchesNumbers
     */
    private function markupElements(string $pattern): array
    {
        $markup = array();
        $currentCount = 0;
        foreach ($this->elements as $symbol) {
            $isMath = $symbol == $pattern;
            if ($isMath) {
                $currentCount++;
            }
            $markup [] = $currentCount;
        }
        return $markup;
    }

}
