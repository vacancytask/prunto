<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 04.11.2017 Time: 1:40
 */

namespace frontend\models\Core;


class Storage implements IStorage
{
    private $attributes = array();

    function setAttribute(string $key, $value)
    {
        $this->attributes[$key] = $value;
    }

    function getAttribute(string $key)
    {
        return $this->attributes[$key];

    }
}
