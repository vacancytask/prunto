<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 04.11.2017 Time: 1:40
 */

namespace frontend\models\Core;


interface IStorage
{

    function setAttribute(string $key, $value);

    function getAttribute(string $key);
}
