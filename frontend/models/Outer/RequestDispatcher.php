<?php
/**
 * prunto
 * © Volkhin Nikolay M., 2017
 * Date: 04.11.2017 Time: 15:30
 */

namespace frontend\models\Outer;

use common\models\LoginForm;
use frontend\models\BusinessProcess\Process;
use frontend\models\BusinessProcess\Product;
use frontend\models\Separator;
use Yii;

class RequestDispatcher
{
    private $jsonAnswer = '';

    public static function login(): string
    {
        $mayLogin = Yii::$app->user->isGuest;

        $answer = 'error';
        $isSuccess = false;
        if ($mayLogin) {
            $model = new LoginForm();
            $isSuccess = $model->load(Yii::$app->request->post()) && $model->login();
        }

        if ($isSuccess) {
            $answer = 'ok';
        }

        return $answer;
    }

    public static function logout(): bool
    {
        $result = Yii::$app->user->logout();

        $answer = 'error';
        if ($result) {
            $answer = 'ok';
        }

        return $answer;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function getSplitWorkId(Separator $model):?int
    {
        $this->performSplit($model);
        $workId = $this->getWorkId();

        return $workId;
    }

    /**
     * @param $model
     */
    private function performSplit(Separator $model)
    {
        $postData = Yii::$app->request->post();
        $hasData = !empty($postData);

        $jsonAnswer = null;
        if ($hasData) {

            $process = new Process();

            $model->load($postData);

            $jsonAnswer = $process->split($model);
        }

        $this->jsonAnswer = $jsonAnswer;
    }

    private function getWorkId(): ?int
    {
        $isValid = !empty($this->jsonAnswer);
        $workId = null;
        if ($isValid) {
            $answer = json_decode($this->jsonAnswer, true);
            $workId = $answer[Product::WORK_ID];
        }
        return $workId;
    }

    /**
     * @param Separator $model
     * @return string
     */
    public function getSplitResult(Separator $model): string
    {
        $this->performSplit($model);

        return $this->jsonAnswer;
    }
}
