<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Symbol array separator';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome</h1>

        <p class="lead">Веб приложение разделения массивов символов ( цифр )</p>

        <p><a class="btn btn-lg btn-success" href="https://bitbucket.org/vacancytask/prunto/wiki/browse/"
              target="_blank">Wiki</a>
        </p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Журнал результатов</h2>

                <p>Просмотр результатов работы приложения</p>

                <p><a class="btn btn-default" href="<?= Url::to(['separator/index']) ?>">Смотреть &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Разделить массив</h2>

                <p>Вам нужно разделить массив на две части, так что бы заданный символ в первой части встречался столько
                    же раз сколько во второй части не встречался ? Вы по адресу ! Жми смелей ! </p>

                <p><a class="btn btn-default" href="<?= Url::to(['separator/create']) ?>">Разделить &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Фреймворк</h2>

                <p>Приложение разработано с минимальными изменениями на основе шаблона "YII 2 advanced"</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/">Yii framework &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
