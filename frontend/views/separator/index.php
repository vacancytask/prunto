<?php

use frontend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SeparatorSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Array split result';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="splitter-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Separate symbols', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'label' => 'Login',
                'value' => function ($model) {
                    $user = User::findOne($model->user_id);

                    $isValid = $user instanceof User;
                    $username = '';
                    if($isValid){
                        $username = $user->username;
                    }

                    return $username;
                },
                'format' => 'html'
            ],
            'symbols:ntext',
            'pattern:ntext',
            'split_at',
            'processed_date',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} или {delete}',],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
