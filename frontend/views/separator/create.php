<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Separator */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Compute symbols split';
$this->params['breadcrumbs'][] = ['label' => 'Separator', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="splitter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="splitter-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'symbols')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'pattern')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
