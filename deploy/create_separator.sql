-- цепляемся к созданной базе ( prunto )
CREATE TABLE separator
(
  id             BIGSERIAL NOT NULL
    CONSTRAINT separator_pkey
    PRIMARY KEY,
  user_id        INTEGER
    CONSTRAINT separator_user_id_fk
    REFERENCES "user",
  symbols        TEXT,
  pattern        TEXT,
  split_at       BIGINT    NOT NULL,
  processed_date TIMESTAMP NOT NULL,
  insert_date    TIMESTAMP DEFAULT now()
);

COMMENT ON TABLE separator IS 'results of array separation';

COMMENT ON COLUMN separator.user_id IS 'Пользователь';

COMMENT ON COLUMN separator.symbols IS 'Массив символов';

COMMENT ON COLUMN separator.pattern IS 'Символ для разбиения';

COMMENT ON COLUMN separator.split_at IS 'Индекс с которого начинается вторая часть';

COMMENT ON COLUMN separator.processed_date IS 'Рассчитано';
