-- цепляемся к созданной базе (prunto) и на неё даём все права созданному пользователю
GRANT ALL ON SCHEMA public TO manager;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO manager;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO manager;
