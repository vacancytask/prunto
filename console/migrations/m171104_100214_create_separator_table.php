<?php

use yii\db\Migration;

/**
 * Handles the creation of table `separator`.
 */
class m171104_100214_create_separator_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('separator', [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->integer(),
            'symbols' => $this->text(),
            'pattern' => $this->text(),
            'split_at' => $this->bigInteger(),
            'processed_date' => $this->datetime(),
        ]);

        $this->createIndex(
            'ix_separator_user_id',
            'separator',
            'user_id'
        );

        $this->addForeignKey(
            'fk_separator_user_id',
            'separator',
            'user_id',
            'user',
            'id',
            'NO ACTION'
        );

        $this->addCommentOnTable('separator', 'results of array separation');

        $this->addCommentOnColumn ( 'separator', 'user_id', 'Пользователь' );
        $this->addCommentOnColumn ( 'separator', 'symbols', 'Массив символов' );
        $this->addCommentOnColumn ( 'separator', 'pattern', 'Символ для разбиения' );
        $this->addCommentOnColumn ( 'separator', 'split_at', 'Индекс с которого начинается вторая часть' );
        $this->addCommentOnColumn ( 'separator', 'processed_date', 'Рассчитано' );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('separator');
    }
}
